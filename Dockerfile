#
FROM centos:centos8
# root
USER root
WORKDIR /root
#
RUN yum -y install epel-release
#
RUN \
  yum -y update && \
  yum -y install \
      jq \
      git \
      tree \
      bash-completion \
      wget \
      sudo \
      make \
      valgrind \
      valgrind-devel.x86_64 \
      gcc \
      gcc-c++ \
      glibc-devel \
      python3-pip
# cmake3
RUN wget https://github.com/Kitware/CMake/releases/download/v3.16.8/cmake-3.16.8-Linux-x86_64.tar.gz \
    && tar -C /usr/local --strip-components=1 --no-same-owner \
           -xvf cmake-3.16.8-Linux-x86_64.tar.gz 
# HDF5
RUN pip3 install h5py
# lwtnn dependencies
RUN \
    dnf --enablerepo=PowerTools -y install \
        eigen3-devel \
        boost-static
# 
WORKDIR /home/atlas/work
# 
COPY build-lwtnn.sh /home/atlas/
#
# assign root privileges to all users, update bashrc file & clean some caches
RUN \
    echo '%sudo   ALL=(ALL)   NOPASSWD: ALL' >> /etc/sudoers \
         '/home/atlas/build-lwtnn.sh' >> ~/.bashrc && \
    rm -rf /var/cache/yum/* && \
    yum clean all
# 
CMD /bin/bash --rcfile ${HOME}/.bashrc