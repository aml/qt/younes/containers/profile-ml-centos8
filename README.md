Docker image for callgrind
==========================

This is some code to build a docker image that contains callgrind and the required tools to build [lwtnn](https://github.com/lwtnn/lwtnn).

Quick Start
===========
Local docker image
------------------

Get the image by running:

```
docker pull gitlab-registry.cern.ch/aml/qt/younes/containers/profile-ml-centos8:latest
docker tag gitlab-registry.cern.ch/aml/qt/younes/containers/profile-ml-centos8:latest profile-ml-centos8:latest
```

Where the second command is a way to alias the full container URL to something which is easier to work with locally.

In principal you can launch an interactive job now with:

```
docker run --rm -it -v ${PWD}/work:/home/atlas/work profile-ml-centos8:latest bash
```

where `-v ${PWD}/work:/home/atlas/work` tells docker to mount `/work` *(a dir located under your current
directory)* in `/home/atlas/work`. **Note that only changes in this directory will be saved when you exit.**


**Thanks to Dan!**